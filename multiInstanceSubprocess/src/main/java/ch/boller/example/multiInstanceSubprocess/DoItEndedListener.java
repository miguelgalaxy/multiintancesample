package ch.boller.example.multiInstanceSubprocess;

import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.ExecutionListener;

public class DoItEndedListener implements ExecutionListener {

	/**
	 * When a breakpoint is added set in this method, an the 
	 * two elements are inside this method at the same time,
	 * an exception is thrown.
	 */
	@Override
	public void notify(DelegateExecution execution) throws Exception {
		System.out.println("hello");
		
	}
}
